/**
 * A jQuery plugin prototype to extend.
 *
 * @author Julien Ramboz <julien.ramboz@gmail.com>
 * @version 1.0.1
 *
 * @example
 *    var MyPlugin = (function(){
 *      PluginPrototype.extend(MyPlugin, PluginPrototype);
 *      MyPlugin.defaultOptions = {};
 *      function MyPlugin() {
 *        MyPlugin.super.constructor.apply(this, arguments);
 *      }
 *      MyPlugin.prototype.myMethod = function(arg1, arg2) {
 *        console.log(arg1, arg2);
 *      }
 *      return MyPlugin;
 *    })();
 *
 *    MyPlugin.install("myPlugin", "ui_", function(){
 *      $(".someClass").ui_myPlugin({option1: "val1", option2: "val2"});
 *      $(".someClass").ui_myPlugin("myMethod", arg1, arg2);
 *    });
 */

// Try loading as AMD or fall back to standard loading order
(function(plugin) {
  if (typeof define === "function" && define.amd) {
    return define("plugin-prototype", ["jquery"], plugin);
  } else {
    return plugin(window.jQuery || window.$);
  }
})(pluginPrototype = function($) {
  "use strict";

  /**
   * @class PluginPrototype
   * @memberOf jQuery.fn
   */
  $.pluginPrototype = window.PluginPrototype = (function() {
    
    /**
     * Create inheritance bewteen classes.
     *
     * @param {Class} child The child class.
     * @param {Class} parent The parent class to extend.
     *
     * @returns {Class} The child class.
     */
    PluginPrototype.extend = function(child, parent) {
      for (var key in parent) {
        if (({}.hasOwnProperty).call(parent, key)) {
          child[key] = parent[key];
        }
      }
      function ctor() {
        this.constructor = child;
      };
      ctor.prototype = parent.prototype;
      child.prototype = new ctor();
      child.super = parent.prototype;
      return child;
    };

    /**
     * Namespace to use as prefix for jQuery plugin.
     *
     * @type {String}
     */
    PluginPrototype.namespace = "ui_";

    /**
     * Default options for the plugin.
     * Remember to override this in the extending classes with:
     *    <class>.defaultOptions = {};
     *
     * @type {Object}
     */
    PluginPrototype.defaultOptions = {};

    /**
     * The constructor for the plugin.
     * Remember to call it from extending classes constructors with:
     *    <class>.super.constructor.apply(this, arguments);
     *
     * @constructs
     */
    function PluginPrototype(el, options) {
      this.el = el;
      this.$el = $(el);
      this.eventNamespace = "." + options.pluginName + "." + PluginPrototype.namespace;
      this.initialize(options);
      this.bindEvents();
    }

    /**
     * Initialize the plugin.
     *
     * @param {Object} options The options for the plugin.
     *
     * @returns {Object} the jQuery selector for the element.
     */
    PluginPrototype.prototype.initialize = function(options) {
      // Reset the plugin if an instance already existed.
      if (this.options) {
        this.reset(this.options);
      }
      this.options = options;
      return this.$el;
    };

    /**
     * Handle events binding.
     */
    PluginPrototype.prototype.bindEvents = function() {
      // this.$el.on("click" + this.eventNamespace, …)
    };

    /**
     * Reset the plugin.
     *
     * @param {Object} options The options to use to reset the plugin.
     */
    PluginPrototype.prototype.reset = function(options) {};

    /**
     * Destroy the plugin.
     *
     * @returns {Object} the jQuery selector for the element the plugin was
     *    bound to.
     */
    PluginPrototype.prototype.destroy = function() {
      var $element;
      // Remove events registered on the namespace
      if (this.$el && PluginPrototype.namespace) {
        this.$el.off(this.eventNamespace);
      }
      $element = this.$el;
      if ($.isFunction($this.removeData)) {
        this.$el.removeData(this.options.pluginName);
      } else {
        this.$el.data(this.options.pluginName, void 0);
      }
      this.el = null;
      this.$el = null;
      this.options = null;
      this.eventNamespace = null;
      return $element;
    };

    /**
     * Install the plugin into jQuery.
     *
     * @param {String} [pluginName] The name to register the plugin as.
     * @param {String} [namespace] The namespace to register the plugin into.
     * @param {Function} [init] The function to call once the plugin is initialized.
     *
     * @returns {Object} The plugin class that was installed.
     */
    PluginPrototype.install = function(pluginName, namespace, init) {
      var pluginClass = this,
          pluginNameFallback = this.name || "pluginPrototype",
          namespaceFallback = PluginPrototype.namespace || "",
          nsPluginName;
      
      // Handle parameters
      if ($.isFunction(pluginName)) {
        init = pluginName;
        pluginName = pluginNameFallback;
      }
      else if ($.isFunction(namespace)) {
        init = namespace;
        namespace = namespaceFallback;
      }
      else {
        // Fallback to the function name of no name is provided.
        pluginName = pluginName || pluginNameFallback;
        // Fallback to the global namespace if none is specified.
        namespace = namespace || namespaceFallback;
      }

      this.prototype.name = pluginName;
      nsPluginName = namespace + pluginName;

      // Register the plugin as jQuery function
      $.fn[nsPluginName] = function() {
        var args = 2 <= arguments.length ? ([].slice).call(arguments, 1) : [],
            options = arguments[0];

        // Call the plugin on each element in the jQuery set
        return this.each(function() {
          var element = this,
              $element = $(this),
              instance,
              pluginOptions;

          // Set default options for the plugin
          pluginOptions = $.extend({}, { pluginName: nsPluginName }, pluginClass.defaultOptions);
          
          // Override default options with data-* attributes if specified
          $.each(pluginOptions, function(option) {
            var value = $element.attr("data-" + option);
            if (value) {
              pluginOptions[$.camelCase(option)] = value;
            }
            return value;
          });

          // Override with JS provided options
          pluginOptions = $.extend({}, pluginOptions, $.isPlainObject(options) ? options : {});

          // If an instance already exists, use it
          instance = $element.data(nsPluginName);

          if (instance) {
            // … and call the corresponding method
            if ($.type(options) === "string") {
              return instance[options].apply(instance, args);
            }
            // … or update the instance with new options
            else if (instance.initialize) {
              return instance.initialize.apply(instance, [pluginOptions].concat(args));
            }
          }
          // … otherwise create a new instance and return it
          else {
            var plugin = new pluginClass($element, pluginOptions, args);
            $element.data(nsPluginName, plugin);
            return plugin;
          }
        });
      };
      // Call the initialization code if any
      if ($.isFunction(init)) {
        $(init);
      }
      return pluginClass;
    };

    return PluginPrototype;

  })();

});
