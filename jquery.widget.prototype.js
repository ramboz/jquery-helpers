/**
 * A jQuery widget prototype to extend.
 *
 * @author Julien Ramboz <julien.ramboz@gmail.com>
 * @version 1.0.1
 *
 * @example
 *    var MyWidget = (function(){
 *      PluginPrototype.extend(MyWidget, WidgetPrototype);
 *      MyWidget.defaultOptions = {};
 *      function MyWidget() {
 *        MyWidget.super.constructor.apply(this, arguments);
 *      }
 *      MyWidget.prototype.bindEvents = function() {
 *        this.handleKeys($el);
 *      }
 *      MyWidget.prototype.accessibility = function() {
 *        this.$el.attr("role", …);
 *      }
 *      MyWidget.prototype.handleEnterKey = function(ev) {
 *        console.log("Enter key!");
 *      };
 *      MyWidget.prototype.myMethod = function(arg1, arg2) {
 *        console.log(arg1, arg2);
 *      }
 *      return MyWidget;
 *    })();
 *
 *    MyWidget.install("myWidget", "ui_", function(){
 *      $(".someClass").ui_myWidget({option1: "val1", option2: "val2"});
 *      $(".someClass").ui_myWidget("myMethod", arg1, arg2);
 *    });
 */

// Try loading as AMD or fall back to standard loading order
(function(plugin) {
  if (typeof define === "function" && define.amd) {
    return define("widget-prototype", ["jquery", "plugin-prototype"], plugin);
  } else {
    return plugin(window.jQuery || window.$, window.PluginPrototype);
  }
})(widgetPrototype = function($, PluginPrototype) {
  "use strict";
  
  /**
   * @class WidgetPrototype
   * @augments PluginPrototype
   * @memberOf jQuery.fn
   */
  $.widgetPrototype = window.WidgetPrototype = (function() {

    // Extend the plugin prototype.
    PluginPrototype.extend(WidgetPrototype, PluginPrototype);

    /**
     * Default options for the widget.
     * Remember to override this in the extending classes with:
     *    <class>.defaultOptions = {};
     *
     * @type {Object}
     */
    WidgetPrototype.defaultOptions = {};

    /**
     * The constructor for the widget.
     * Remember to call it from extending classes constructors with:
     *    <class>.super.constructor.apply(this, arguments);
     *
     * @constructs
     */
    function WidgetPrototype(element, options) {
      WidgetPrototype.super.constructor.apply(this, arguments);
      this.accessibility();
    }

    /**
     * Set accessibility states and properties for the widget.
     */
    WidgetPrototype.prototype.accessibility = function() {};

    /**
     * Handle key events for the widget.
     *
     * @param {Object} $el The jQuery selector to bind the listener to.
     */
    WidgetPrototype.prototype.handleKeys = function($el) {
      var self = this;
      var $element = $el || this.$el;
      return $element.on("keydown" + this.eventNamespace, function(ev) {
        switch (e.keyCode || e.which) {
          case 9:
            return self.handleTabKey(e);
          case 13:
            return self.handleEnterKey(e);
          case 32:
            return self.handleSpaceKey(e);
          case 27:
            return self.handleEscapeKey(e);
          case 37:
            return self.handleLeftKey(e);
          case 38:
            return self.handleUpKey(e);
          case 39:
            return self.handleRightKey(e);
          case 40:
            return self.handleDownKey(e);
          case 33:
            return self.handlePageUpKey(e);
          case 34:
            return self.handlePageDownKey(e);
          case 36:
            return self.handleHomeKey(e);
          case 35:
            return self.handleEndKey(e);
          case 106:
            return self.handleAsteriskKey(e);
          default:
            if (e.keyCode >= 48 && e.keyCode <= 57) {
              return self.handleNumberKey(e);
            } else if (e.keyCode >= 65 && e.keyCode <= 90) {
              return self.handleLetterKey(e);
            }
        };
      });
    };

    /**
     * Handle `Tab` key events.
     *
     * @param {Object} ev The event that was fired.
     */
    WidgetPrototype.prototype.handleTabKey = function(ev) {};

    /**
     * Handle `Enter` key events.
     *
     * @param {Object} ev The event that was fired.
     */
    WidgetPrototype.prototype.handleEnterKey = function(ev) {};

    /**
     * Handle `Space` key events.
     *
     * @param {Object} ev The event that was fired.
     */
    WidgetPrototype.prototype.handleSpaceKey = function(ev) {
      return this.keyEnter(ev);
    };

    /**
     * Handle `Escape` key events.
     *
     * @param {Object} ev The event that was fired.
     */
    WidgetPrototype.prototype.handleEscapeKey = function(ev) {};

    /**
     * Handle `Left` arrow key events.
     *
     * @param {Object} ev The event that was fired.
     */
    WidgetPrototype.prototype.handleLeftKey = function(ev) {};

    /**
     * Handle `Right` arrow key events.
     *
     * @param {Object} ev The event that was fired.
     */
    WidgetPrototype.prototype.handleRightKey = function(ev) {};

    /**
     * Handle `Up` arrow key events.
     *
     * @param {Object} ev The event that was fired.
     */
    WidgetPrototype.prototype.handleUpKey = function(ev) {};

    /**
     * Handle `Down` arrow key events.
     *
     * @param {Object} ev The event that was fired.
     */
    WidgetPrototype.prototype.handleDownKey = function(ev) {};

    /**
     * Handle `Page Up` key events.
     *
     * @param {Object} ev The event that was fired.
     */
    WidgetPrototype.prototype.handlePageUpKey = function(ev) {};

    /**
     * Handle `Page Down` key events.
     *
     * @param {Object} ev The event that was fired.
     */
    WidgetPrototype.prototype.handlePageDownKey = function(ev) {};

    /**
     * Handle `Home` key events.
     *
     * @param {Object} ev The event that was fired.
     */
    WidgetPrototype.prototype.handleHomeKey = function(ev) {};

    /**
     * Handle `End` key events.
     *
     * @param {Object} ev The event that was fired.
     */
    WidgetPrototype.prototype.handleEndKey = function(ev) {};

    /**
     * Handle `Asterisk` key events.
     *
     * @param {Object} ev The event that was fired.
     */
    WidgetPrototype.prototype.handleAsteriskKey = function(ev) {};

    /**
     * Handle `Number` key events.
     *
     * @param {Object} ev The event that was fired.
     */
    WidgetPrototype.prototype.handleNumberKey = function(ev) {};

    /**
     * Handle `Letter` key events.
     *
     * @param {Object} ev The event that was fired.
     */
    WidgetPrototype.prototype.handleLetterKey = function(ev) {};;

    return WidgetPrototype;

  })();
  
});
