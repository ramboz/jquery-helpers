jQuery Helpers
==============

A set scripts that help in the creation of jQuery plugins and widgets.

Features
--------

- Provide a prototype jQuery plugin and widget
- Each script is an AMD module with standard loading fallback
- Installs as jQuery plugin: `$(".myClass").myPlugin()`
- Plugins/widgets has API for both initialization and method calls:
	- Initialize/reset:
		`$(".myClass").myPlugin({option1: value1, option2, value2})`
	- Method calls:
		`$(".myClass").myPlugin("myMethod", arg1, arg2)`
- Options can be specified in 3 different ways:
	- defaults: as static object on the plugin
		`MyPlugin.defaultOptions: {option1: value1, option2: value2}`
	- data attributes:
		`<div class="myClass" data-option1="value1" data-option2="value2">`
	- constructor options:
		`$(".myClass").myPlugin({option1: value1, option2, value2})`

Usage for plugin
----------------
```
#!javascript
// Plugin definition
var MyPlugin = (function(){
  PluginPrototype.extend(MyPlugin, PluginPrototype);
  MyPlugin.defaultOptions = {};
  function MyPlugin() {
    MyPlugin.super.constructor.apply(this, arguments);
  }
  MyPlugin.prototype.myMethod = function(arg1, arg2) {
    console.log(arg1, arg2);
  }
  return MyPlugin;
})();

// Install into jQuery
MyPlugin.install("myPlugin", "ui_", function(){
  $(".someClass").ui_myPlugin({option1: "val1", option2: "val2"});
  $(".someClass").ui_myPlugin("myMethod", arg1, arg2);
});
```

Usage for widget
----------------
```
#!javascript
// Widget definition
var MyWidget = (function(){
  PluginPrototype.extend(MyWidget, WidgetPrototype);
  MyWidget.defaultOptions = {};
  function MyWidget() {
    MyWidget.super.constructor.apply(this, arguments);
  }
  MyWidget.prototype.bindEvents = function() {
    this.handleKeys($el);
  }
  MyWidget.prototype.accessibility = function() {
    this.$el.attr("role", …);
  }
  MyWidget.prototype.handleEnterKey = function(ev) {
    console.log("Enter key!");
  };
  MyWidget.prototype.myMethod = function(arg1, arg2) {
    console.log(arg1, arg2);
  }
  return MyWidget;
})();

// Install into jQuery
MyWidget.install("myWidget", "ui_", function(){
  $(".someClass").ui_myWidget({option1: "val1", option2: "val2"});
  $(".someClass").ui_myWidget("myMethod", arg1, arg2);
});
```
